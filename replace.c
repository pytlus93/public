#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *strrep(char *input,char *sub,char *rep)
{
	int i =0, j=0, k=0, n=0, m=0,znaku;
	char *result;

	znaku = strlen(input)*strlen(rep);
	result = (char*)malloc(sizeof(char)*znaku);
	for(i=0;i<strlen(input)+1;i++)
	{
		if(input[i]==sub[0])
		{			
			k=1;
			n=i+1;
			while(n<strlen(input)+1)
			{
				if(k>=strlen(sub) || sub[k]!=input[n])
				{
					if(k==strlen(sub))
						for(m=0;m<strlen(rep);m++)
							result[j++] = rep[m];
					else
						for(m=0;m<k;m++)
							result[j++] = input[i++];
					i=n-1;					
					break;
				}
				k++;
				n++;
			}
		}
		else
		{
			result[j++] = input[i];
			continue;
		}		
	}
	result[j++] = 0;
	return result;
}

int main(int argc, char *argv[])
{
	char *vstup = "sisinsinicsin\0";	
	char *vystup = strrep(vstup,"sin","A");
	printf("%s\n",strrep("a b c d a b c d","a","e"));		
	printf("zadani: %s\nreseni: %s\nreseni: %s\n",vstup,vystup,strrep(vystup,"A","sin"));
	getchar();
	return(0);	
}

